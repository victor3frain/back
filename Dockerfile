FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as build
WORKDIR /app
COPY . .
RUN dotnet restore
RUN dotnet publish -o /app/published-app

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine as runtime
RUN apk add --no-cache icu-libs
RUN apk add --no-cache --upgrade bash
RUN apk add --update nodejs npm
RUN npm install -g newman
RUN npm install -g newman-reporter-htmlextra
RUN mkdir Files
RUN mkdir Files/Collections
RUN mkdir Files/Reports
RUN mkdir Files/Scripts
RUN echo "#!/bin/bash \nnewman run -k $1 -r htmlextra --reporter-htmlextra-export $2" > Files/Scripts/test_curl.sh
RUN chmod 755 Files -R
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

WORKDIR /app
COPY --from=build /app/published-app /app
ENTRYPOINT [ "dotnet", "/app/Coe_Ticket.dll" ]

EXPOSE 80

