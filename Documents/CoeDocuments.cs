using System;
using Encrypt_Decrypt;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utils;

namespace Coe_Ticket
{

    public class CoeDocuments
    {
        private CoeDocumentsDB Documents;
        private Decrypt ed;
        private Response response;
        private string ConnectionString;

        public CoeDocuments(string ConnectionString)
        {
            this.response = new Response();
            this.ConnectionString = ConnectionString;
            Documents = new CoeDocumentsDB(ConnectionString);
            ed = new Decrypt();
        }

        public void GetSpecTemplates(dynamic Deploy)
        {
            dynamic Specifications = Deploy.Specifications;
            string query = "{\"$or\": [TEMPLATE_NAME]}";
            string findingJson = "{\"Name\": \"OT_Header\"},";
            var specDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(Specifications));
            foreach (string specName in specDictionary.Keys)
            {
                if (!string.IsNullOrEmpty(specName))
                {
                    findingJson += "{\"Name\": \"" + specName + "_Template\"},";
                }
            }
            findingJson = findingJson.Substring(0, findingJson.Length - 1);
            query = query.Replace("TEMPLATE_NAME", findingJson);
            List<dynamic> DocumentList = this.Documents.GetDocument(query);

            Dictionary<string, dynamic> Documents = new Dictionary<string, dynamic>();
            foreach (dynamic document in DocumentList)
            {
                Documents.Add(document.Name, document);
            }

            SetBodyResponse("Create the template", "", CreateOrderDocument(Documents, specDictionary, Deploy));
        }

        private string CreateOrderDocument(Dictionary<string, dynamic> DocumentTemplates, Dictionary<string, dynamic> Specifications, dynamic Deploy)
        {
            string orderExecution = CreateOTHeader(ed.DecryptBase64(DocumentTemplates["OT_Header"].Content), Deploy);
            dynamic spec;

            if (Specifications.ContainsKey("TLS"))
            {
                spec = Specifications["TLS"];
                if (spec.Count > 0)
                    orderExecution += CreateTLSTemplate(DocumentTemplates["TLS_Template"], spec);
            }

            if (Specifications.ContainsKey("JWT"))
            {
                spec = Specifications["JWT"];
                if (spec.Count > 0)
                    orderExecution += CreateJWTTemplate(DocumentTemplates["JWT_Template"], spec);
            }

            JObject ca = new JObject();
            if (Specifications.ContainsKey("CA"))
            {
                spec = Specifications["CA"];
                if (spec.ContainsKey("ambiente") && spec["ambiente"].Count > 0)
                {
                    JArray specAmb = Specifications["CA"]["ambiente"];
                    ca.Add("ambiente", specAmb);
                    orderExecution += CreateCATemplate(DocumentTemplates["CA_Template"], ca);
                    Specifications["CA"].Remove("ambiente");
                    if (!Specifications["CA"].ContainsKey("componentes") &&
                    !Specifications["CA"].ContainsKey("app") &&
                    !Specifications["CA"].ContainsKey("app-key") &&
                    !Specifications["CA"].ContainsKey("producto"))
                        Specifications.Remove("CA");
                }
            }

            if (Specifications.ContainsKey("API"))
            {
                spec = Specifications["API"];
                orderExecution += CreateAPITemplate(DocumentTemplates["API_Template"], spec);
            }

            if (Specifications.ContainsKey("CA"))
            {
                ca = new JObject();
                spec = Specifications["CA"];

                if (spec.ContainsKey("componentes") && spec["componentes"].Count > 0)
                    ca.Add("componentes", spec["componentes"]);

                if (spec.ContainsKey("app") && spec["app"].Count > 0)
                    ca.Add("app", spec["app"]);

                if (spec.ContainsKey("app-key") && spec["app-key"].Count > 0)
                    ca.Add("app-key", spec["app-key"]);

                if (spec.ContainsKey("producto") && spec["producto"].Count > 0)
                    ca.Add("producto", spec["producto"]);


                orderExecution += CreateCATemplate(DocumentTemplates["CA_Template"], ca);
            }

            if (Specifications.ContainsKey("reverseStrategy"))
                orderExecution += CreateRSemplate(ed.DecryptBase64(DocumentTemplates["reverseStrategy_Template"].Content), Convert.ToString(Specifications["reverseStrategy"]));

            return orderExecution;
        }


        private string CreateRSemplate(string rsTemplate, string ReverseStrategy)
        {
            return rsTemplate.Replace("[REVERSE_PLAN]", ReverseStrategy);
        }

        private string CreateOTHeader(string OT_Header, dynamic Deploy)
        {
            string template = OT_Header.Replace("[Explicit Descripción]", Deploy.description);
            template = template.Replace("[Developer_Default]", Deploy.developer != null ? Deploy.developer.name + " " + Deploy.developer.lastName : string.Empty);
            return template;
        }

        private string CreateTLSTemplate(dynamic TLS, dynamic TLS_Specification)
        {
            string template = ed.DecryptBase64(TLS.Content);

            int startingTemplateIndex = template.IndexOf("[START_MODIFY]");
            string templateEditingArea = template.Substring(startingTemplateIndex);

            template = template.Replace(templateEditingArea, string.Empty);

            foreach (dynamic tls_spec in TLS_Specification)
            {
                template += templateEditingArea;
                template = template.Replace("[TARGETNAME]", tls_spec.targetName.Value);
                template = template.Replace("[ADDINGFIRSTCURL]", tls_spec.CURLs.AddingFirstCurl.Value);
                template = template.Replace("[ADDINGSECONDCURL]", tls_spec.CURLs.AddingSecondCurl.Value);
                template = template.Replace("[ADDINGTHIRDCURL]", tls_spec.CURLs.AddingThirdCurl.Value);

                if (!string.IsNullOrEmpty(tls_spec.CURLs.DeletingReference.Value) && !string.IsNullOrEmpty(tls_spec.CURLs.DeletingKS.Value))
                {

                    template = template.Replace("[DELETEINGKS]", tls_spec.CURLs.DeletingReference.Value);
                    template = template.Replace("[DELETINGREFERENCE]", tls_spec.CURLs.DeletingKS.Value);
                }
                else
                {
                    int intialIndex = template.IndexOf("[DELETETLS-START]");
                    int finalIndex = template.IndexOf("[DELETETLS-END]");
                    string deleteTLS = Substring(template, intialIndex, finalIndex);
                    template = template.Replace(deleteTLS, string.Empty);
                }

                //Enviroment
                template = template.Replace("[ENVIROMENT]", GetEnviroment(tls_spec.enviroment.Value));

                template = template.Replace("[NOTES]", tls_spec.notes.Value);
                template = template.Replace("[DELETETLS-END]", string.Empty);
                template = template.Replace("[DELETETLS-START]", string.Empty);
                template = template.Replace("[START_MODIFY]", string.Empty);
                template = template.Replace("[END_MODIFY]", string.Empty);
            }

            return template;
        }

        private string CreateJWTTemplate(dynamic JWT, dynamic JWT_Specification)
        {
            string template = ed.DecryptBase64(JWT.Content);

            int startingTemplateIndex = template.IndexOf("[START_MODIFY]");
            string templateEditingArea = template.Substring(startingTemplateIndex);

            template = template.Replace(templateEditingArea, string.Empty);
            string enviromentValue = string.Empty;
            foreach (dynamic jwt_spec in JWT_Specification)
            {
                template += templateEditingArea;
                template = template.Replace("[APPNAME]", jwt_spec.appName.Value);

                //Enviroment
                if (Convert.ToBoolean(jwt_spec["enviroments"]["prodInt"]) || Convert.ToBoolean(jwt_spec["enviroments"]["prodExt"]))
                {
                    enviromentValue = "AWS (";
                    if (Convert.ToBoolean(jwt_spec["enviroments"]["prodInt"]))
                        enviromentValue += "Prod-Int, ";
                    if (Convert.ToBoolean(jwt_spec["enviroments"]["prodExt"]))
                        enviromentValue += "Prod-Ext, ";
                    enviromentValue = enviromentValue.Substring(0, enviromentValue.Length - 2);
                    enviromentValue += ") ";
                }
                if (Convert.ToBoolean(jwt_spec["enviroments"]["prodIntOn"]) || Convert.ToBoolean(jwt_spec["enviroments"]["prodExtOn"]))
                {
                    enviromentValue = "On Premise (";
                    if (Convert.ToBoolean(jwt_spec["enviroments"]["prodIntOn"]))
                        enviromentValue += "Prod-Int, ";
                    if (Convert.ToBoolean(jwt_spec["enviroments"]["prodExtOn"]))
                        enviromentValue += "Prod-Ext, ";
                    enviromentValue = enviromentValue.Substring(0, enviromentValue.Length - 2);
                    enviromentValue += ") ";
                }
                template = template.Replace("[ENVIROMENT]", enviromentValue);

                template = template.Replace("[APINAME]", jwt_spec.apiName.Value);

                template = template.Replace("[FIRSTCURL]", jwt_spec.curls.firstCurl.Value);
                template = template.Replace("[SECONDCURL]", jwt_spec.curls.secondCurl.Value);
                template = template.Replace("[THIRDCURL]", jwt_spec.curls.thirdCurl.Value);

                template = template.Replace("[JWTNOTES]", jwt_spec.details.Value);

                template = template.Replace("[START_MODIFY]", string.Empty);
                template = template.Replace("[END_MODIFY]", string.Empty);
            }

            return template;
        }

        private string CreateCATemplate(dynamic CA, dynamic CA_Specification)
        {
            string template = ed.DecryptBase64(CA.Content);
            int startingTemplateIndex = template.IndexOf("[START_MODIFY]");
            string templateEditingArea = template.Substring(startingTemplateIndex);
            string title = template.Substring(0, startingTemplateIndex);
            template = string.Empty;

            string enviromentsValue;
            string templateCA = string.Empty;
            string nameCA = string.Empty;
            foreach (JProperty ca_spec in CA_Specification)
            {
                nameCA = ca_spec.Name;
                template += title.Replace("CATYPE", nameCA);
                foreach (JObject ca in CA_Specification[nameCA])
                {
                    enviromentsValue = string.Empty;
                    if (ca.ContainsKey("type"))
                        templateCA = templateEditingArea.Replace("[CA_TYPE_DEPLOY]", Convert.ToString(ca["type"]).ToUpper() == "NEW" ? "Nuevo" : "Actualización");
                    else
                        templateCA = templateEditingArea.Replace("[CA_TYPE_DEPLOY]", "Tipo de despliegue no definido");

                    templateCA = templateCA.Replace("[CANAME]", Convert.ToString(ca["name"]));
                    templateCA = templateCA.Replace("[CAURL]", Convert.ToString(ca["repo"]));
                    templateCA = templateCA.Replace("[CAORIGIN]", Convert.ToString(ca["origin"]));
                    templateCA = templateCA.Replace("[CANOTES]", Convert.ToString(ca["details"]));
                    if (Convert.ToBoolean(ca["enviroments"]["prodInt"]) || Convert.ToBoolean(ca["enviroments"]["prodExt"]))
                    {
                        enviromentsValue = "AWS (";
                        if (nameCA.ToUpper() == "AMBIENTE" && (Convert.ToBoolean(ca["enviroments"]["prodInt"]) || Convert.ToBoolean(ca["enviroments"]["prodExt"])))
                        {
                            enviromentsValue += "Prod-Int, Prod-Ext,";
                        }
                        else if (nameCA.ToUpper() != "AMBIENTE" && (Convert.ToBoolean(ca["enviroments"]["prodInt"]) || Convert.ToBoolean(ca["enviroments"]["prodExt"])))
                        {
                            if (Convert.ToBoolean(ca["enviroments"]["prodInt"]))
                                enviromentsValue += "Prod-Int, ";
                            else if (Convert.ToBoolean(ca["enviroments"]["prodExt"]))
                                enviromentsValue += "Prod-Ext, ";
                        }
                        else
                        {
                            if (Convert.ToBoolean(ca["enviroments"]["prodInt"]))
                                enviromentsValue += "Prod-Int, ";
                            if (Convert.ToBoolean(ca["enviroments"]["prodExt"]))
                                enviromentsValue += "Prod-Ext, ";
                        }
                        enviromentsValue = enviromentsValue.Substring(0, enviromentsValue.Length - 2);
                        enviromentsValue += ") ";
                    }
                    if (Convert.ToBoolean(ca["enviroments"]["prodIntOn"]) || Convert.ToBoolean(ca["enviroments"]["prodExtOn"]))
                    {
                        enviromentsValue = "On Premise (";
                        if (nameCA.ToUpper() == "AMBIENTE" && (Convert.ToBoolean(ca["enviroments"]["prodIntOn"]) || Convert.ToBoolean(ca["enviroments"]["prodExtOn"])))
                        {
                            enviromentsValue += "Prod-Int, Prod-Ext, ";
                        }
                        else if (nameCA.ToUpper() != "AMBIENTE" && (Convert.ToBoolean(ca["enviroments"]["prodIntOn"]) || Convert.ToBoolean(ca["enviroments"]["prodExtOn"])))
                        {
                            if (Convert.ToBoolean(ca["enviroments"]["prodIntOn"]))
                                enviromentsValue += "Prod-Int, ";
                            else if (Convert.ToBoolean(ca["enviroments"]["prodExtOn"]))
                                enviromentsValue += "Prod-Ext, ";
                        }
                        else
                        {
                            if (Convert.ToBoolean(ca["enviroments"]["prodIntOn"]))
                                enviromentsValue += "Prod-Int, ";
                            if (Convert.ToBoolean(ca["enviroments"]["prodExtOn"]))
                                enviromentsValue += "Prod-Ext, ";
                        }
                        enviromentsValue = enviromentsValue.Substring(0, enviromentsValue.Length - 2);
                        enviromentsValue += ") ";
                    }
                    templateCA = templateCA.Replace("[CADESTINY]", enviromentsValue);
                    template += templateCA;
                }
            }

            template = template.Replace("[START_MODIFY]", string.Empty);
            template = template.Replace("[END_MODIFY]", string.Empty);
            return template;
        }

        private string CreateAPITemplate(dynamic API, dynamic API_Specification)
        {
            string template = ed.DecryptBase64(API.Content);
            int startingTemplateIndex = template.IndexOf("[START_MODIFY]");
            string templateEditingArea = template.Substring(startingTemplateIndex);
            template = template.Substring(0, startingTemplateIndex);

            string enviromentsValue;
            string templateAPI = string.Empty;
            string nameAPI = string.Empty;

            foreach (JObject api_spec in API_Specification)
            {
                enviromentsValue = string.Empty;
                templateAPI = templateEditingArea.Replace("[CANAME]", Convert.ToString(api_spec["name"]));
                templateAPI = templateAPI.Replace("[CAURL]", Convert.ToString(api_spec["repo"]));
                templateAPI = templateAPI.Replace("[CAORIGIN]", Convert.ToString(api_spec["origin"]));
                templateAPI = templateAPI.Replace("[CANOTES]", Convert.ToString(api_spec["details"]));
                if (Convert.ToBoolean(api_spec["enviroments"]["prodInt"]) || Convert.ToBoolean(api_spec["enviroments"]["prodExt"]))
                {
                    enviromentsValue = "AWS (";
                    if (Convert.ToBoolean(api_spec["enviroments"]["prodInt"]))
                        enviromentsValue += "Prod-Int, ";
                    if (Convert.ToBoolean(api_spec["enviroments"]["prodExt"]))
                        enviromentsValue += "Prod-Ext, ";

                    enviromentsValue = enviromentsValue.Substring(0, enviromentsValue.Length - 2);
                    enviromentsValue += ") ";
                }
                if (Convert.ToBoolean(api_spec["enviroments"]["prodIntOn"]) || Convert.ToBoolean(api_spec["enviroments"]["prodExtOn"]))
                {
                    enviromentsValue = "On Premise (";
                    if (Convert.ToBoolean(api_spec["enviroments"]["prodIntOn"]))
                        enviromentsValue += "Prod-Int, ";
                    if (Convert.ToBoolean(api_spec["enviroments"]["prodExtOn"]))
                        enviromentsValue += "Prod-Ext, ";

                    enviromentsValue = enviromentsValue.Substring(0, enviromentsValue.Length - 2);
                    enviromentsValue += ") ";
                }
                templateAPI = templateAPI.Replace("[CADESTINY]", enviromentsValue);
                templateAPI = templateAPI.Replace("[TYPE]", Convert.ToString(api_spec["type"]) == "new" ? "NUEVA" : "ACTUALIZACIÓN");
                template += templateAPI;
            }

            template = template.Replace("[START_MODIFY]", string.Empty);
            template = template.Replace("[END_MODIFY]", string.Empty);
            return template;
        }


        private string GetEnviroment(string speEnviroment)
        {
            string enviroment = string.Empty;
            switch (speEnviroment.ToUpper())
            {
                case "A1":
                    enviroment = "PROD-INT";
                    break;
                case "A2":
                    enviroment = "PROD-EXT";
                    break;
                case "A3":
                    enviroment = "PROD-INT-ONPREMISE";
                    break;
                case "A4":
                    enviroment = "PROD-EXT-ONPREMISE";
                    break;
            }
            return enviroment;
        }

        private string Substring(string Value, int initialindex, int finalIndex)
        {
            string substractionValue = string.Empty;
            for (int index = initialindex; index < finalIndex; index++)
            {
                substractionValue += Value[index];
            }
            return substractionValue;
        }

        private void GetDocumentString(string name)
        {
            string query = "{\"Name\":\"" + name + "\"}";
            List<dynamic> documents = this.Documents.GetDocument(query);
            string result = string.Empty;

            if (documents.Count == 0)
                result = "Error al obtener el documento";
            else
            {
                dynamic template = Convert.ToString(documents[0].Content);
                result = ed.DecryptBase64(template);
            }
            SetBodyResponse("Getting encoded document", "", result);
        }

        private void SetBodyResponse(string Message, string user, dynamic Body)
        {
            this.response.message = Message;
            this.response.user = user;
            this.response.status = 200;
            this.response.body = Body;
        }

        public Response ExecuteMethod(string Resource, dynamic Params)
        {
            try
            {
                switch (Resource.ToUpper())
                {
                    case "GETSPECTEMPLATES":
                        GetSpecTemplates(Params);
                        break;
                    case "GETDOCUMENTSTRING":
                        GetDocumentString(Params);
                        break;
                    default:
                        return "Method: " + Resource.ToUpper() + ", Parameters: " + Convert.ToString((Params));
                }
            }
            catch (Exception ex)
            {
                this.response.error = ex.Message;
                this.response.status = 500;
            }
            return this.response;
        }
    }
}