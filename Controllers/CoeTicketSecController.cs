using System;
using System.Dynamic;
using Microsoft.AspNetCore.Mvc;
using Encrypt_Decrypt;
using Newtonsoft.Json.Linq;
using Utils;
using System.Net.Mime;

namespace Coe_Ticket
{

    [ApiController]
    [Route("[controller]")]
    public class CoeTicketSecController : ControllerBase
    {
        private readonly ILogger<CoeTicketSecController> _logger;
        protected readonly IConfiguration _configuration;
        private string cn;
        private Decrypt decrypt;

        public CoeTicketSecController(ILogger<CoeTicketSecController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            cn = configuration.GetSection("AppSettings").GetSection("AppConnection").Value;
            decrypt = new Decrypt();
        }

        [HttpGet]
        public dynamic Get()
        {
            return "CoeTicketSecController online";
        }

        #region Login
        [Route("login/authenticate/{UsrEmail}/{UsrPassword}/{RememberUsr}")]
        [HttpGet]
        public dynamic AuthenticateUser(string UsrEmail, string UsrPassword, bool RememberUsr)
        {
            dynamic Params = new ExpandoObject();
            Params.Email = decrypt.DecryptBase64(UsrEmail);
            Params.Password = decrypt.DecryptBase64(UsrPassword);
            Params.RememberUser = RememberUsr;
            return ExecuteByClass("LOGIN", "Authenticate", Params);
        }

        [Route("login/checksession/{usuario}")]
        [HttpGet]
        public dynamic CheckSession(string usuario)
        {
            dynamic Params = new ExpandoObject();
            Params.Usuario = decrypt.DecryptBase64(usuario);
            return ExecuteByClass("LOGIN", "CheckSession", Params);
        }

        [Route("login/logout")]
        [HttpPut]
        public dynamic LogOut([FromBody] dynamic data)
        {
            string param = Convert.ToString(data);
            dynamic paramObject = JObject.Parse(Convert.ToString(param));
            return ExecuteByClass("LOGIN", "LOGOUT", paramObject);
        }
        #endregion

        [Route("user/info/{user}")]
        [Produces(MediaTypeNames.Application.Json)]
        [HttpGet]
        public dynamic GetInfoUser(string user)
        {
            dynamic User = new ExpandoObject();
            User.Email = user;
            return ExecuteByClass("USER", "GetInfoUser", User);
        }

        [Route("user/devops")]
        [Produces(MediaTypeNames.Application.Json)]
        [HttpGet]
        public dynamic GetDevOpsUsers(){
            return ExecuteByClass("USER", "GetDevOpsUsers", new Object());
        }

        [Route("user/developers/{name}")]
        [Produces(MediaTypeNames.Application.Json)]
        [HttpGet]
        public dynamic GetDeveloperUsers(string name){
            dynamic developer = new ExpandoObject();
            developer.name = decrypt.DecryptBase64(name);
            return ExecuteByClass("USER", "GetDeveloperUsers", developer);
        }

        [Route("user/create")]
        [Produces(MediaTypeNames.Application.Json)]
        [HttpPost]
        public dynamic CreateUser([FromBody] dynamic DataUser){
            string param = Convert.ToString(DataUser);
            dynamic paramObject = JObject.Parse(Convert.ToString(param));
            return ExecuteByClass("USER", "CREATEUSER", paramObject);
        }

        private dynamic ExecuteByClass(string FirstResource, string Resource, dynamic Params)
        {
            dynamic result;
            switch (FirstResource.ToUpper())
            {
                case "LOGIN":
                    CoeLogin coeLogin = new CoeLogin(cn);
                    result = coeLogin.ExecuteMethod(Resource.ToUpper(), Params);
                    break;
                case "USER":
                    UserInfo userInfo = new UserInfo(cn);
                    result = userInfo.ExecuteMethod(Resource.ToUpper(), Params);
                    break;
                default:
                    result = "Class: " + FirstResource.ToUpper() + ", Resource: " + Resource + ", Parameters: " + Params;
                    break;
            }
            return result;
        }
    }
}