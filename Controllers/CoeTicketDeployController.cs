using Microsoft.AspNetCore.Mvc;
using System.Dynamic;
using Newtonsoft.Json.Linq;

namespace Coe_Ticket
{

    [ApiController]
    [Route("[controller]")]
    public class CoeTicketDeployController : ControllerBase
    {
        private readonly ILogger<CoeTicketDeployController> _logger;
        protected readonly IConfiguration _configuration;
        private string cn;

        public CoeTicketDeployController(ILogger<CoeTicketDeployController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            cn = configuration.GetSection("AppSettings").GetSection("AppConnection").Value;
        }

        [HttpGet]
        public dynamic Get()
        {
            return "CoeTicketDeployController online";
        }

        [HttpGet("deploy/period/{initialdate}/{finaldate}")]
        public dynamic GetDeploys(DateTime initialdate, DateTime finaldate)
        {
            dynamic period = new ExpandoObject();
            period.InitialDate = initialdate;
            period.FinalDate = finaldate;
            return ExecuteByClass("Deploy", "GetDeployList", period);
        }

        [Route("deploy/update")]
        [HttpPut]
        public dynamic UpdateDeploy([FromBody] dynamic data)
        {
            string param = Convert.ToString(data);
            dynamic paramObject = JObject.Parse(Convert.ToString(param));
            return ExecuteByClass("DEPLOY", "UPDATEDEPLOY", paramObject);
        }

        [Route("deploy/create")]
        [HttpPost]
        public dynamic Create([FromBody] dynamic data)
        {
            string param = Convert.ToString(data);
            dynamic paramObject = JObject.Parse(Convert.ToString(param));
            return ExecuteByClass("DEPLOY", "CREATE", paramObject);
        }

        [HttpGet("deploy/getone/{id}")]
        public dynamic GetDeploy(string id)
        {
            dynamic paramObject = new ExpandoObject();
            paramObject.id = id;
            return ExecuteByClass("DEPLOY", "GETDEPLOY", paramObject);
        }

        [HttpDelete("deploy/delete/{id}")]
        public dynamic DeleteDeploy(string id)
        {
            dynamic paramObject = new ExpandoObject();
            paramObject.id = id;
            return ExecuteByClass("DEPLOY", "DELETE", paramObject);
        }

        [Route("deploy/setSpec")]
        [HttpPut]
        public dynamic UpdateSpec([FromBody] dynamic data)
        {
            string param = Convert.ToString(data);
            dynamic paramObject = JObject.Parse(Convert.ToString(param));
            return ExecuteByClass("DEPLOY", "UpdateSpec", paramObject);
        }

        [Route("deploy/getspec/{id}")]
        [HttpGet]
        public dynamic GetSpec(string id)
        {
            dynamic paramObject = new ExpandoObject();
            paramObject.id = id;
            return ExecuteByClass("DEPLOY", "GETSPECIFICATIONS", paramObject);
        }

        [Route("deploy/getDocumentSpec/{id}")]
        [HttpGet]
        public dynamic GetSpecDocument(string id)
        {
            dynamic paramObject = new ExpandoObject();
            paramObject.id = id;
            return ExecuteByClass("DEPLOY", "GETSPECDOCUMENT", paramObject);
        }
        
        [Route("deploy/getExecutionOrder/{date}")]
        [HttpGet]
        public dynamic GetExecutionOrder(string date)
        {
            dynamic paramObject = new ExpandoObject();
            paramObject.searchingDate = date;
            return ExecuteByClass("DEPLOY", "GETEXECUTIONORDER", paramObject);
        } 

        [Route("deploy/testcollection")]
        [HttpPost]
        public dynamic GetTestingCollectionResult([FromBody] dynamic Collection){
            dynamic Params = new ExpandoObject();
            Params.Collection = Collection;
            Params.PathFiles = _configuration.GetSection("AppSettings").GetSection("PathFiles").Value;
            return ExecuteByClass("DEPLOY", "TESTCOLLECTION", Params);
        }

        private dynamic ExecuteByClass(string FirstResource, string Resource, dynamic Params)
        {
            CoeDeploy coeDeploy = new CoeDeploy(cn);
            dynamic result;
            switch (FirstResource.ToUpper())
            {
                case "DEPLOY":
                    result = coeDeploy.ExecuteMethod(Resource.ToUpper(), Params);
                    break;
                default:
                    result = "Class: " + FirstResource.ToUpper() + ", Resource: " + Resource + ", Parameters: " + Params;
                    break;
            }
            return result;
        }
    }
}