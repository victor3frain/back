using Microsoft.AspNetCore.Mvc;
using Encrypt_Decrypt;
using System.Dynamic;

namespace Coe_Ticket
{

    [ApiController]
    [Route("[controller]")]
    public class CoeTicketGeneralController : ControllerBase
    {
        private readonly ILogger<CoeTicketGeneralController> _logger;
        protected readonly IConfiguration _configuration;
        private string cn;
        private Decrypt decrypt;

        public CoeTicketGeneralController(ILogger<CoeTicketGeneralController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            cn = configuration.GetSection("AppSettings").GetSection("AppConnection").Value;
            decrypt = new Decrypt();
        }

        [HttpGet]
        public dynamic Get()
        {
            return "CoeTicketSecController online";
        }

        [HttpOptions]
        public dynamic GetVersion()
        {
            dynamic response = new ExpandoObject();
            response.version = "1.13.0";
            response.lastMOdification = "31-05-2022 12:52";
            return response;
        }
    }
}