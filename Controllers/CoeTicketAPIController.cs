using Microsoft.AspNetCore.Mvc;
using System.Dynamic;


namespace Coe_Ticket
{

    [ApiController]
    [Route("[controller]")]
    public class CoeTicketAPIController : ControllerBase
    {
        private readonly ILogger<CoeTicketAPIController> _logger;
        protected readonly IConfiguration _configuration;
        private string cn;

        public CoeTicketAPIController(ILogger<CoeTicketAPIController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            cn = configuration.GetSection("AppSettings").GetSection("AppConnection").Value;
        }

        [HttpGet]
        public dynamic Get()
        {
            return "CoeTicketAPIController online";
        }

        [HttpGet]
        [Route("api/getapilist/{pageSize}/{pagenumber}")]
        public dynamic GetAPIList(int pagesize, int pagenumber)
        {
            dynamic QueryInfo = new ExpandoObject();
            QueryInfo.skip = pagenumber;
            QueryInfo.limit = pagesize;
            return ExecuteByClass("API", "GetAPIList", QueryInfo);
        }

        private dynamic ExecuteByClass(string FirstResource, string Resource, dynamic Params)
        {
            CoeAPI coeDeploy = new CoeAPI(cn);
            dynamic result;
            switch (FirstResource.ToUpper())
            {
                case "API":
                    result = coeDeploy.ExecuteMethod(Resource.ToUpper(), Params);
                    break;
                default:
                    result = "Class: " + FirstResource.ToUpper() + ", Resource: " + Resource + ", Parameters: " + Params;
                    break;
            }
            return result;
        }
    }
}