using System.Dynamic;
using System.Numerics;
using Encrypt_Decrypt;
using System.Globalization;
using System.IO;
using Utils;
using System.Text.RegularExpressions;

namespace Coe_Ticket
{
    public class CoeDeploy
    {
        private CoeDeployDB Deploy;
        private Decrypt ed;
        private Response response;
        private string ConnectionString;
        public CoeDeploy(string ConnectionString)
        {
            this.response = new Response();
            this.ConnectionString = ConnectionString;
            Deploy = new CoeDeployDB(ConnectionString);
            ed = new Decrypt();
        }

        private void GetDeployList(DateTime InitialDate, DateTime FinalDate)
        {
            SetBodyResponse("Get Deploy List", string.Empty, this.Deploy.GetDeployList(InitialDate, FinalDate));
        }

        private void GetDeploy(string id)
        {
            SetBodyResponse("Get Deploy List", string.Empty, this.Deploy.GetDeploy(id));
        }

        private void UpdateDeploy(dynamic UpdatedDeploy)
        {
            dynamic Deploy = new ExpandoObject();
            Deploy.id = Convert.ToString(UpdatedDeploy.id);

            if (UpdatedDeploy.title != null)
                Deploy.title = Convert.ToString(UpdatedDeploy.title);

            if (UpdatedDeploy.developer1 != null)
                Deploy.developer1 = Convert.ToString(UpdatedDeploy.developer1);

            if (UpdatedDeploy.developer2 != null)
                Deploy.developer2 = Convert.ToString(UpdatedDeploy.developer2);

            if (UpdatedDeploy.titleOt != null)
                Deploy.titleOt = Convert.ToString(UpdatedDeploy.titleOt);

            if (UpdatedDeploy.jira != null)
                Deploy.jira = Convert.ToString(UpdatedDeploy.jira);

            if (UpdatedDeploy.date != null)
                Deploy.date = DateTime.Parse(Convert.ToString(UpdatedDeploy.date)).ToUniversalTime();
            else if (UpdatedDeploy.start != null)
                Deploy.date = DateTime.Parse(Convert.ToString(UpdatedDeploy.start)).ToUniversalTime();

            if (UpdatedDeploy.status != null)
                Deploy.status = Convert.ToInt32(UpdatedDeploy.status);
            else if (UpdatedDeploy.ContainsKey("extendedProps"))
                if (UpdatedDeploy.extendedProps.status != null)
                    Deploy.status = Convert.ToInt32(UpdatedDeploy.extendedProps.status);

            if (UpdatedDeploy.otNumber != null)
                Deploy.otNumber = Convert.ToString(UpdatedDeploy.otNumber);

            if (UpdatedDeploy.description != null)
                Deploy.description = Convert.ToString(UpdatedDeploy.description);

            if (UpdatedDeploy.link != null)
                Deploy.link = Convert.ToString(UpdatedDeploy.link);

            if (UpdatedDeploy.team != null)
                Deploy.team = Convert.ToInt32(UpdatedDeploy.team);

            if (UpdatedDeploy.end != null)
            {
                Deploy.finalDate = DateTime.Parse(Convert.ToString(UpdatedDeploy.end)).ToUniversalTime();
            }

            if (UpdatedDeploy.devops != null)
            {
                Deploy.devops = new ExpandoObject();
                Deploy.devops.id = Convert.ToInt32(UpdatedDeploy.devops.id);
                Deploy.devops.name = Convert.ToString(UpdatedDeploy.devops.name);
                Deploy.devops.lastName = Convert.ToString(UpdatedDeploy.devops.lastName);
            }

            if (UpdatedDeploy.notes != null)
                Deploy.notes = Convert.ToString(UpdatedDeploy.notes);

            if (UpdatedDeploy.reverseStrategy != null)
                Deploy.reverseStrategy = Convert.ToString(UpdatedDeploy.reverseStrategy);


            if (UpdatedDeploy.buildingStatus != null)
                Deploy.buildingStatus = Convert.ToInt32(UpdatedDeploy.buildingStatus);

            SetBodyResponse("Update deploy", string.Empty, this.Deploy.UpdateDeploy(Deploy));
        }

        private void CreateDeploy(dynamic NewDeploy)
        {
            dynamic Deploy = new ExpandoObject();
            Deploy._id = Convert.ToString(new BigInteger(Guid.NewGuid().ToByteArray()));
            Deploy.title = Convert.ToString(NewDeploy.title);
            Deploy.titleOt = Convert.ToString(NewDeploy.titleOt);
            Deploy.jira = Convert.ToString(NewDeploy.jira);
            Deploy.otNumber = string.IsNullOrEmpty(Convert.ToString(NewDeploy.otNumber)) ? null : Convert.ToInt32(NewDeploy.otNumber);
            Deploy.description = Convert.ToString(NewDeploy.description);
            Deploy.date = null;
            Deploy.link = Convert.ToString(NewDeploy.link);
            Deploy.team = string.IsNullOrEmpty(Convert.ToString(NewDeploy.team)) ? null : Convert.ToInt32(NewDeploy.team);
            Deploy.developer = Convert.ToString(NewDeploy.developer);
            Deploy.devops = Convert.ToString(NewDeploy.devops);
            Deploy.status = string.IsNullOrEmpty(Convert.ToString(NewDeploy.status)) ? 2 : Convert.ToInt32(NewDeploy.status);
            Deploy.buildingStatus = string.IsNullOrEmpty(Convert.ToString(NewDeploy.buildingStatus)) ? 0 : Convert.ToInt32(NewDeploy.buildingStatus);
            Deploy.notes = Convert.ToString(NewDeploy.notes);
            SetBodyResponse("Get Deploy List", string.Empty, this.Deploy.CreateDeploy(Deploy));
        }

        private void DeleteDeploy(string idDeploy)
        {
            SetBodyResponse("Delete Deploy", string.Empty, this.Deploy.DeleteDeploy(idDeploy));
        }

        private void UpdateSpec(dynamic Spec)
        {
            dynamic spec = ObjectManagement.JObject2Dynamic(Spec);
            dynamic originalSpec = this.Deploy.GetSpecifications(spec.id);

            if (originalSpec != null && ObjectManagement.ExistsProperty(originalSpec, "CA") && ObjectManagement.ExistsProperty(spec.Specifications, "CA"))
            {
                originalSpec.CA = ObjectManagement.MergeDynamicObjects(originalSpec.CA, spec.Specifications.CA);
                spec.Specifications.CA = originalSpec.CA;
            }

            if (originalSpec != null)
                spec.Specifications = ObjectManagement.MergeDynamicObjects(originalSpec, spec.Specifications);

            SetBodyResponse("Update Specifications", string.Empty, this.Deploy.UpdateDeploy(spec));
        }

        private void GetSpec(string id)
        {
            SetBodyResponse("Get Deploy List", string.Empty, this.Deploy.GetSpecifications(id));
        }

        #region GettingDocuments
        private void GetSpecDocument(string id)
        {
            dynamic Deploy = new ExpandoObject();
            Deploy = this.Deploy.GetDeploy(id);

            if (!((IDictionary<String, object>)Deploy).ContainsKey("Specifications"))
                Deploy.Specifications = null;
            else if (ObjectManagement.ExistsProperty(Deploy, "reverseStrategy"))
                ObjectManagement.AddProperty2Object(Deploy.Specifications, "reverseStrategy", Deploy.reverseStrategy);

            SetBodyResponse("Create the template", "", GetDocumentString(Deploy));
        }

        private string GetDocumentString(dynamic Deploy)
        {
            CoeDocuments documents = new CoeDocuments(ConnectionString);
            return documents.ExecuteMethod("GETSPECTEMPLATES", Deploy).body;
        }

        private string GetDocumentString(string name)
        {
            CoeDocuments documents = new CoeDocuments(ConnectionString);
            return documents.ExecuteMethod("GETDOCUMENTSTRING", name).body;
        }
        #endregion

        #region ExecutionOrder
        private void GetExecutionOrder(string SearchingDate)
        {
            DateTime initialDate = Convert.ToDateTime(SearchingDate);
            DateTime finalDate = Convert.ToDateTime(SearchingDate);
            finalDate = finalDate.AddHours(23);
            List<dynamic> deploys = this.Deploy.GetDeployList(initialDate, finalDate).acceptedDeploys;
            dynamic templateExecution;
            if (deploys.Count > 0)
            {
                templateExecution = GetDocumentString("Execution_Order_Template");
                templateExecution = SetExecutionOrder(deploys, templateExecution);
            }
            else
            {
                templateExecution = "<h1>No existen OTs agendadas este día</h1>";
            }
            SetBodyResponse("Create the template", "", templateExecution);
        }

        private string SetExecutionOrder(List<dynamic> Deploys, string executionOrderTemplate)
        {
            Deploys = Deploys.OrderBy(deploy => deploy.title).ToList();

            //Formatted Date
            CultureInfo culture = new CultureInfo("es-ES");
            string formattedDate = Deploys[0].date.ToString("dd MMMM yy", culture);
            executionOrderTemplate = executionOrderTemplate.Replace("<!--Date-->", formattedDate);

            //Title Order
            int initialIndexTemplate = executionOrderTemplate.IndexOf("<!--InitialTitleOt-->");
            string deployTitleTemplate = executionOrderTemplate.Substring(initialIndexTemplate, executionOrderTemplate.IndexOf("<!--FinalTitleOt-->") - initialIndexTemplate);
            executionOrderTemplate = executionOrderTemplate.Replace(deployTitleTemplate, "<!--DeployTitleArea-->");
            executionOrderTemplate = executionOrderTemplate.Replace("<!--FinalTitleOt-->", string.Empty);
            string deployTitleArea = string.Empty;
            Regex rx = new Regex(@"^((\d){2}|(\d){1}). ");
            foreach (dynamic deploy in Deploys)
            {
                deployTitleArea += deployTitleTemplate.Replace("<!--Title-->", rx.Match(deploy.title).Success ? Convert.ToString(deploy.title).Substring(deploy.title.IndexOf(" ")) : "[Formato no incluido \"Número. \"] " + deploy.title);
                deployTitleArea = deployTitleArea.Replace("<!--InitialOTs-->", string.Empty);
            }
            executionOrderTemplate = executionOrderTemplate.Replace("<!--DeployTitleArea-->", deployTitleArea);

            //Assign DevOps
            executionOrderTemplate = executionOrderTemplate.Replace("<!--DevOpsName-->", Deploys[0].devops == null ? "*No asignado*" : Deploys[0].devops.name + " " + Deploys[0].devops.lastName);

            //Deploy details
            initialIndexTemplate = executionOrderTemplate.IndexOf("<!--InitialOTs-->");
            deployTitleTemplate = executionOrderTemplate.Substring(initialIndexTemplate, executionOrderTemplate.IndexOf("<!--FinalOTs-->") - initialIndexTemplate);
            executionOrderTemplate = executionOrderTemplate.Replace(deployTitleTemplate, "<!--DeployTitleArea-->");
            executionOrderTemplate = executionOrderTemplate.Replace("<!--FinalOTs-->", string.Empty);
            deployTitleArea = string.Empty;
            foreach (dynamic deploy in Deploys)
            {
                deployTitleArea += deployTitleTemplate.Replace("<!--TitleBuisness-->", !ObjectManagement.ExistsProperty(deploy, "title") ? string.Empty : rx.Match(deploy.title).Success ? Convert.ToString(deploy.title).Substring(deploy.title.IndexOf(" ")) : "[Formato no incluido \"Número. \"] " + deploy.title);
                deployTitleArea = deployTitleArea.Replace("<!--TitleOT-->", !ObjectManagement.ExistsProperty(deploy, "titleOt") ? deploy.title : Convert.ToString(deploy.titleOt));
                deployTitleArea = deployTitleArea.Replace("<!--OtNumber-->", !ObjectManagement.ExistsProperty(deploy, "otNumber") ? string.Empty : Convert.ToString(deploy.otNumber));
                deployTitleArea = deployTitleArea.Replace("<!--JiraCode-->", !ObjectManagement.ExistsProperty(deploy, "jira") ? string.Empty : Convert.ToString(deploy.jira));
                deployTitleArea = deployTitleArea.Replace("<!--DeveloperNight-->", !ObjectManagement.ExistsProperty(deploy, "developer1") ? "No asignado" : Convert.ToString(deploy.developer1));
                deployTitleArea = deployTitleArea.Replace("<!--DeveloperMorning-->", !ObjectManagement.ExistsProperty(deploy, "developer2") ? "No asignado" : Convert.ToString(deploy.developer2));
                deployTitleArea = deployTitleArea.Replace("<!--InitialOTs-->", string.Empty);
            }
            executionOrderTemplate = executionOrderTemplate.Replace("<!--DeployTitleArea-->", deployTitleArea);

            return executionOrderTemplate;
        }

        #endregion

        #region TestingCURLS
        public void TestCollection(dynamic Collection, string basePath)
        {
            //Set Collection name
            string todayString = DateTime.UtcNow.ToString("yyyy-MM-ddTHH_mm_ss");
            dynamic result;
            string scriptPath = Path.Combine(basePath, "Files", "Scripts", "test_curl.sh");
            string fileName = "collection_" + todayString;
            string collectionPath = Path.Combine(basePath, "Files", "Collections", fileName + ".json");
            string reportpath = Path.Combine(basePath, "Files", "Reports", fileName + ".html");

            dynamic Result = FileManagement.CreateTXTFile(Path.Combine(basePath, "Files", "Collections"), fileName + ".json", Collection.ToString());
            if (Result.result == 1)
            {
                string parameters = collectionPath + " " + reportpath;
                result = Utils.UtilSystem.ExecuteCommand(scriptPath, parameters, 60000);
                result.success = Utils.FileManagement.GetContentFile(reportpath);
                Utils.FileManagement.DeleteFile(collectionPath);
                Utils.FileManagement.DeleteFile(reportpath);
            }
            else
            {
                result = "archivo no generado " + ObjectManagement.ExistsProperty(Result, "error") ? "(" + Result.error + ")" : string.Empty;
            }
            SetBodyResponse("Testing CURLS", "", result);
        }

        #endregion

        private void SetBodyResponse(string Message, string user, dynamic Body)
        {
            this.response.message = Message;
            this.response.user = user;
            this.response.status = 200;
            this.response.body = Body;
        }

        public Response ExecuteMethod(string Resource, dynamic Params)
        {
            try
            {
                switch (Resource.ToUpper())
                {
                    case "GETDEPLOYLIST":
                        GetDeployList(Params.InitialDate, Params.FinalDate);
                        break;
                    case "UPDATEDEPLOY":
                        UpdateDeploy(Params);
                        break;
                    case "CREATE":
                        CreateDeploy(Params);
                        break;
                    case "GETDEPLOY":
                        GetDeploy(Convert.ToString(Params.id));
                        break;
                    case "DELETE":
                        DeleteDeploy(Convert.ToString(Params.id));
                        break;
                    case "UPDATESPEC":
                        UpdateSpec(Params);
                        break;
                    case "GETSPECIFICATIONS":
                        GetSpec(Convert.ToString(Params.id));
                        break;
                    case "GETSPECDOCUMENT":
                        GetSpecDocument(Params.id);
                        break;
                    case "GETEXECUTIONORDER":
                        GetExecutionOrder(Params.searchingDate);
                        break;
                    case "TESTCOLLECTION":
                        TestCollection(Params.Collection, Params.PathFiles);
                        break;
                    default:
                        return "Method: " + Resource.ToUpper() + ", Parameters: " + Convert.ToString((Params));
                }
            }
            catch (Exception ex)
            {
                this.response.error = ex.Message;
                this.response.status = 500;
            }
            return this.response;
        }
    }
}