using System.Dynamic;

namespace Coe_Ticket;

public class Response
{
    public string error { get; set; }
    public string message { get; set; }
    public dynamic data { get; set; }
    public int status { get; set; }
    public dynamic body { get; set; }

    public string user {get; set;}

    public Response()
    {
        this.error = string.Empty;
        this.message = string.Empty;
        this.data = new ExpandoObject();
        this.status = 0;
        this.body = new ExpandoObject();
        this.user = string.Empty;
    }
}