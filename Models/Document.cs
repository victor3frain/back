using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Coe_Ticket
{

    public class Document
    {
        [BsonElement("_id")]
        public ObjectId _id { get; set; } = ObjectId.GenerateNewId();

        [BsonElement("Name")]
        public string Name { get; set; } = string.Empty;

        [BsonElement("Content")]
        public string Content { get; set; } = string.Empty;

        [BsonElement("Type")]
        public string Type { get; set; } = string.Empty;

        [BsonElement("LastModification")]
        public DateTime LastModification { get; set; } = new DateTime();

        [BsonElement("LastModification")]
        public string User {get; set;} = string.Empty;

        public Document(){}

    }
}