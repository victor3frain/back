namespace Coe_Ticket
{
    public class Notification
    {
        public string User { get; set; } = string.Empty;
        public dynamic NotificationBody { get; set; } = new Object();

        public string UserId {get; set;} = string.Empty;
    }

}