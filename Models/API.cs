using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

public class API
{
    [BsonElement("_id")]
    public ObjectId _id { get; set; } = ObjectId.GenerateNewId();

    [BsonElement("gitlab")]
    public BsonElement gitlab { get; set; } = new BsonElement();

    [BsonElement("apigee")]
    public BsonElement apigee { get; set; } = new BsonElement();

    [BsonElement("name")]
    public string Name { get; set; } = string.Empty;

    [BsonElement("microservice")]
    public string Content { get; set; } = string.Empty;

    [BsonElement("lastactivity")]

    public DateTime LastModification { get; set; } = new DateTime();

    public API() { }
}