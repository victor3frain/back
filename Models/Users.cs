using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Coe_Ticket
{
    public class Users
    {

        [BsonElement("_id")]
        public ObjectId _id { get; set; } = ObjectId.GenerateNewId();

        [BsonElement("username")]
        public string username { get; set; } = string.Empty;

        [BsonElement("password")]
        public string password { get; set; } = string.Empty;

        [BsonElement("email")]
        public string email { get; set; } = string.Empty;

        [BsonElement("name")]
        public string name { get; set; } = string.Empty;

        [BsonElement("lastaname")]
        public string lastName { get; set; } = string.Empty;

        [BsonElement("login")]
        public bool login { get; set; } = false;

        [BsonElement("rememberLogin")]
        public bool rememberLogin { get; set; } = false;

        [BsonElement("ip")]
        public string ip { get; set; } = string.Empty;

        [BsonElement("role")]
        public string role { get; set; } = string.Empty;

        [BsonElement("team")]
        public string team { get; set; } = string.Empty;

        public Users() { }
        public Users(ObjectId _id, string username, string password, string email, string name, string lastName, bool login, bool rememeberLogin, string ip)
        {
            this._id = _id;
            this.username = username;
            this.password = password;
            this.email = email;
            this.name = name;
            this.lastName = lastName;
            this.login = login;
            this.rememberLogin = rememberLogin;
            this.ip = ip;
        }
    }
}