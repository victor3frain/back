using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Coe_Ticket
{
    public class Deploys
    {
        [BsonElement("_id")]
        public ObjectId _id { get; set; } = ObjectId.GenerateNewId();

        [BsonElement("title")]
        public string title { get; set; } = string.Empty;

        [BsonElement("description")]
        public string description { get; set; } = string.Empty;

        [BsonElement("date")]
        public DateTime date { get; set; } = new DateTime();

        [BsonElement("link")]
        public string link { get; set; } = string.Empty;

        [BsonElement("team")]
        public int team { get; set; } = -1;

        [BsonElement("developer")]
        public string developer { get; set; } = string.Empty;

        [BsonElement("devops")]
        public string devops { get; set; } = string.Empty;

        [BsonElement("status")]
        public int status { get; set; } = -1;


        [BsonElement("buildingStatus")]
        public int buildingStatus { get; set; } = -1;

        public Deploys() { }
        public Deploys(ObjectId _id, string title, string description, DateTime date, string link, int team, string developer, string devops, int status, int buildingStatus)
        {
            this._id = _id;
            this.title = title;
            this.description = description;
            this.date = date;
            this.link = link;
            this.team = team;
            this.developer = developer;
            this.devops = devops;
            this.status = status;
            this.buildingStatus = buildingStatus;
        }
    }
}