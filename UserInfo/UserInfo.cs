using System;
using System.Dynamic;
using DB_Connection;
using Encrypt_Decrypt;

namespace Coe_Ticket
{

    public class UserInfo
    {
        private CoeUsersDB User;
        private Decrypt ed;
        private Response response;
        public UserInfo(string ConnectionString)
        {
            this.response = new Response();
            User = new CoeUsersDB(ConnectionString);
            ed = new Decrypt();
        }

        private void GetInfoUser(dynamic User)
        {
            string email = User.Email == "-1" ? User.Email : ed.DecryptBase64(User.Email);
            dynamic Object = new ExpandoObject();
            if (email != "-1")
                Object.User = this.User.GetInfoUser("email", email);
            else
                Object.User = this.User.GetInfoUser("ip", Utils.UtilsNet.GetIpAddress());

            if (Object.User != null)
            {
                Object.User.password = null;
            }
            SetResponseBody("User info", email, Object);
        }

        private void GetDevOpsUsers(){
            SetResponseBody("DevOps info", null, this.User.GetDevOpsUser());
        }

        private void GetDeveloperUsers(dynamic developer){
            SetResponseBody("DevOps info", null, this.User.GetDeveloperUsers(developer.name));
        }

        private void CreateUser(dynamic DataUser){
            SetResponseBody("Create User", Convert.ToString(DataUser.Name), this.User.CreateUser(DataUser));
        }

        private void SetResponseBody(string Message, string User, dynamic Body)
        {
            this.response.message = Message;
            this.response.user = User;
            this.response.status = 200;
            this.response.body = Body;
        }

        public Response ExecuteMethod(string Resource, dynamic Params)
        {
            try
            {
                switch (Resource.ToUpper())
                {
                    case "GETINFOUSER":
                        GetInfoUser(Params);
                        break;
                    case "GETDEVOPSUSERS":
                        GetDevOpsUsers();
                    break;
                    case "GETDEVELOPERUSERS":
                        GetDeveloperUsers(Params);
                    break;
                    case "CREATEUSER":
                        CreateUser(Params);
                    break;
                    default:
                        return "Method: " + Resource.ToUpper() + ", Parameters: " + Convert.ToString((Params));
                }
            }
            catch (Exception ex)
            {
                this.response.error = ex.Message;
                this.response.status = 500;
            }
            return this.response;
        }
    }
}