using System;
using System.Dynamic;
using MongoDB.Bson;
using Utils;

namespace Coe_Ticket
{
    public class CoeDeployDB
    {

        private DB_Connection.MongoDB mongodb;
        public CoeDeployDB(string connectionString)
        {
            mongodb = new DB_Connection.MongoDB(connectionString, new Deploys());
            mongodb.InitializeDataBase("coetickets_dev");
        }

        #region Getting Methods
        public dynamic GetDeployList(DateTime InitialDate, DateTime FinalDate)
        {
            mongodb.InitializeCollection("COE_Deploys");

            BsonDocument deploySearching = new BsonDocument
            {
                {"date", new BsonDocument{
                    {"$gte", InitialDate},
                    {"$lte", FinalDate}
                }},
                {"status", new BsonDocument{
                    {"$in", new BsonArray{1, 2}}
                }}
            };

            dynamic deployList = new ExpandoObject();
            deployList.acceptedDeploys = mongodb.FindingList(deploySearching);

            deploySearching = new BsonDocument
            {
                {"status",  new BsonDocument{
                    {"$in", new BsonArray{0, 3, 4, 5}}
                }}
            };
            deployList.pendingDeploys = mongodb.FindingList(deploySearching);
            return deployList;
        }

        public dynamic GetDeploy(string id)
        {
            mongodb.InitializeCollection("COE_Deploys");
            dynamic deploy = mongodb.FindOne("{\"_id\":\"" + id + "\"}");
            return deploy;
        }

        public dynamic GetSpecifications(string id)
        {
            mongodb.InitializeCollection("COE_Deploys");
            dynamic deploy = mongodb.FindOne("{\"_id\":\"" + id + "\"}");
            if (!((IDictionary<String, object>)deploy).ContainsKey("Specifications"))
                deploy.Specifications = null;
            else if (ObjectManagement.ExistsProperty(deploy, "reverseStrategy"))
                ObjectManagement.AddProperty2Object(deploy.Specifications, "reverseStrategy", deploy.reverseStrategy);

            return deploy.Specifications;
        }

        #endregion

        #region Updating Methods
        public dynamic UpdateDeploy(dynamic UpdatedDeploy)
        {
            try
            {
                mongodb.InitializeCollection("COE_Deploys");
                string deploySearching = "{\"_id\": \"" + UpdatedDeploy.id + "\"}";
                UpdatedDeploy.id = null;
                mongodb.Update(deploySearching, UpdatedDeploy);
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        #endregion

        #region Creating Methods
        public dynamic CreateDeploy(dynamic NewDeploy)
        {
            try
            {
                mongodb.InitializeCollection("COE_Deploys");
                mongodb.Create(NewDeploy);
                return NewDeploy._id;
            }
            catch
            {
                return -1;
            }
        }
        #endregion

        #region Deleting Methods
        public dynamic DeleteDeploy(string idDeploy)
        {
            try
            {
                mongodb.InitializeCollection("COE_Deploys");
                mongodb.Delete("{\"_id\": \"" + idDeploy + "\"}");
                return 1;
            }
            catch
            {
                return -1;
            }
        }
        #endregion
    }
}