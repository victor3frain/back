using System;
using System.Dynamic;
using MongoDB.Bson;
using Utils;

namespace Coe_Ticket
{
    public class CoeDocumentsDB
    {
        private DB_Connection.MongoDB mongodb;
        public CoeDocumentsDB(string connectionString)
        {
            mongodb = new DB_Connection.MongoDB(connectionString, new Document());
            mongodb.InitializeDataBase("coetickets_dev");
            mongodb.InitializeCollection("COE_Documents");
        }

        #region Getting Methods
        public dynamic GetDocument(string SearchingJson){
            dynamic documents = this.mongodb.FindingList(SearchingJson);
            return documents;
        }
        #endregion
    }
}