using System;
using System.Dynamic;
using Utils;
using Encrypt_Decrypt;

namespace Coe_Ticket
{
    public class CoeUsersDB
    {

        private DB_Connection.MongoDB mongodb;
        private Encrypt ed;
        private Decrypt de;
        public CoeUsersDB(string connectionString)
        {
            mongodb = new DB_Connection.MongoDB(connectionString, new Users());
            mongodb.InitializeDataBase("coetickets_dev");
            mongodb.InitializeCollection("COE_Users");
            ed = new Encrypt();
            de = new Decrypt();
        }

        public dynamic AuthenticateUser(string Email, string Password, bool RememberUser)
        {
            string filter = "{email: \"" + Email + "\", password: \"" + ed.StaticEncrypt(Password) + "\"}";
            dynamic user = this.mongodb.FindOne(filter);

            dynamic result = new ExpandoObject();
            result.login = user != null;
            if (result.login != null)
            {
                dynamic updatedObject = new ExpandoObject();
                updatedObject.ip = UtilsNet.GetIpAddress();
                updatedObject.rememberLogin = RememberUser;
                updatedObject.login = true;
                this.mongodb.Update(filter, updatedObject);
            }
            result.user = user;
            return result;
        }

        public dynamic CheckSession(string searchingProperty, string searchingValue)
        {
            dynamic result = new ExpandoObject();
            if (!string.IsNullOrEmpty(searchingProperty))
            {
                string filter = "{\"" + searchingProperty + "\":\"" + searchingValue + "\"}";
                result.User = this.mongodb.FindOne(filter);
                if (result.User != null)
                {
                    result.ipSession = result.User.ip == UtilsNet.GetIpAddress();
                    result.session = result.User.rememberLogin || result.User.login;
                }
                else
                    result.session = false;
            }
            else
                result.session = false;

            return result;
        }

        public dynamic LogOut(string searchingValue)
        {
            string filter = "{\"email\":\"" + searchingValue + "\"}";
            dynamic updatedObject = new ExpandoObject();
            updatedObject.ip = string.Empty;
            updatedObject.rememberLogin = false;
            updatedObject.login = false;
            this.mongodb.Update(filter, updatedObject);
            return true;
        }

        public dynamic GetInfoUser(string searchingField, string searchingValue)
        {
            string filter = "{\"" + searchingField + "\": \"" + searchingValue + "\"}";
            return this.mongodb.FindOne(filter);
        }

        public dynamic GetDevOpsUser()
        {
            string filter = "{\"team\":0}";
            List<dynamic> devOpsUsers = this.mongodb.FindingList(filter);
            List<dynamic> devOpsList = new List<dynamic>();
            foreach (dynamic devOpsUser in devOpsUsers)
            {
                dynamic user = new ExpandoObject();
                user.id = devOpsUser._id;
                user.name = devOpsUser.name;
                user.lastName = devOpsUser.lastName;
                devOpsList.Add(user);
            }
            devOpsList = devOpsList.OrderBy(p => p.name).ToList();
            return devOpsList;
        }

        public dynamic GetDeveloperUsers(string name)
        {
            string filter = "{\"name\": /.*" + name + ".*/, \"team\": {\"$gt\": 0}}";
            List<dynamic> developerUsers = this.mongodb.FindingList(filter, 0, 5);
            List<dynamic> developerList = new List<dynamic>();
            foreach (dynamic developerUser in developerUsers)
            {
                dynamic user = new ExpandoObject();
                user.id = developerUser._id;
                user.name = developerUser.name;
                user.lastName = developerUser.lastName;
                developerList.Add(user);
            }
            developerList = developerList.OrderBy(p => p.name).ToList();
            return developerList;
        }

        public dynamic CreateUser(dynamic DataUser)
        {   
            string dom = Convert.ToString(DataUser.DevOpsEmail);
            string dop = Convert.ToString(DataUser.DevOpsPassword);
            if(string.IsNullOrEmpty(dom) || string.IsNullOrEmpty(dop)){
                throw new Exception("Información de usuario DevOps no encontrada");
            }

            dynamic userAuthenticated = this.mongodb.FindOne("{\"email\":\""+dom+"\", \"password\":\""+ed.StaticEncrypt(dop)+"\"}");
            if (userAuthenticated != null){
                string name = Convert.ToString(DataUser.Name);
                string lastname = Convert.ToString(DataUser.LastName);
                dynamic user = new ExpandoObject();

                user._id = this.mongodb.GetCountDocuments("{\"_id\": {\"$gt\":-1}}") + 2;
                user.username = name.Split(" ")[0].ToLower() + "." + lastname.Split(" ")[0].ToLower();
                user.email = Convert.ToString(DataUser.Email);
                user.name = name;
                user.lastName = lastname;
                user.role = Convert.ToString(DataUser.Role).ToLower();

                if (string.IsNullOrEmpty(Convert.ToString(DataUser.Password)))
                {
                    user.password = ed.StaticEncrypt(name.Split(" ")[0].ToLower() + lastname.Split(" ")[0].ToLower() + "001");
                }
                else
                {
                    user.password = ed.StaticEncrypt(Convert.ToString(DataUser.Password));
                }

                user.rememberLogin = false;
                user.ip = string.Empty;
                user.login = false;
                user.team = Convert.ToInt32(DataUser.Team);

                this.mongodb.Create(user);

                return "Usuario creado";
            }
            else{
                throw new Exception("Usuario DevOps no autenticado");
            }
        }
    }
}