using System;
using System.Dynamic;
using MongoDB.Bson;
using Utils;

namespace Coe_Ticket
{
    public class CoeAPIDB
    {

        private DB_Connection.MongoDB mongodb;
        public CoeAPIDB(string connectionString)
        {
            mongodb = new DB_Connection.MongoDB(connectionString, new API());
            mongodb.InitializeDataBase("coetickets_dev");
            mongodb.InitializeCollection("COE_API");
        }

        #region Getting Methods
        public List<dynamic> GetAPIList(int skip, int limit, string filter)
        {
            string showedFields = "{name: 1, microservice: 1, apigee: 1}";
            List<dynamic> apiList = mongodb.FindingList(filter, skip, limit, showedFields);
            apiList = apiList.Select(api => { api._id = api._id.ToString(); return api; }).ToList();
            return apiList;
        }

        public dynamic GetAPI(string id)
        {
            dynamic api = mongodb.FindOne(id);
            return api;
        }

        #endregion

        #region Updating Methods
        public dynamic UpdateAPI(dynamic UpdatedDeploy)
        {
            dynamic result = new ExpandoObject();
            return result;
        }
        #endregion

        #region Creating Methods
        public dynamic CreateDeploy(dynamic NewAPI)
        {
            dynamic result = new ExpandoObject();
            return result;
        }
        #endregion

        #region Deleting Methods
        public dynamic DeleteDeploy(string idAPI)
        {
            dynamic result = mongodb.Delete(idAPI);
            return result;
        }
        #endregion
    }
}