using System.Dynamic;
using Encrypt_Decrypt;
using Utils;

namespace Coe_Ticket
{
    public class CoeLogin
    {
        private CoeUsersDB User;
        private Decrypt ed;
        private Response response;

        public CoeLogin(string ConnectionString)
        {
            this.response = new Response();
            User = new CoeUsersDB(ConnectionString);
            ed = new Decrypt();
        }

        private void AuthenticateUser(string Email, string Password, bool rememberUser)
        {
            SetBodyResponse("Authenticated User", Email, this.User.AuthenticateUser(Email.Trim(), Password, rememberUser));
        }

        private void LogOut(string User)
        {
            dynamic body = new ExpandoObject();
            body.success = this.User.LogOut(User);
            SetBodyResponse("Log Out", User, body);
        }

        private void CheckSession(dynamic Params)
        {
            if (string.IsNullOrEmpty(Params.Usuario))
            {
                string ip = UtilsNet.GetIpAddress();
                SetBodyResponse("Check Session", ip, this.User.CheckSession("ip", ip));
            }
            else{
                SetBodyResponse("Check Session", Params.Usuario, this.User.CheckSession("email", Params.Usuario));
            }
        }

        private void SetBodyResponse(string Message, string user, dynamic Body)
        {
            this.response.message = Message;
            this.response.user = user;
            this.response.status = 200;
            this.response.body = Body;
        }

        public Response ExecuteMethod(string Resource, dynamic Params)
        {
            try
            {
                switch (Resource.ToUpper())
                {
                    case "AUTHENTICATE":
                        AuthenticateUser(Params.Email, Params.Password, Params.RememberUser);
                        break;
                    case "LOGOUT":
                        string user = string.IsNullOrEmpty(Convert.ToString(Params.data)) ? string.Empty : ed.DecryptBase64(Convert.ToString(Params.data));
                        LogOut(user);
                        break;
                    case "CHECKSESSION":
                        CheckSession(Params);
                        break;
                    default:
                        return "Method: " + Resource.ToUpper() + ", Parameters: " + Convert.ToString((Params));
                }
            }
            catch (Exception ex)
            {
                this.response.error = ex.Message;
                this.response.status = 500;
            }
            return this.response;
        }
    }
}