using System.Dynamic;
using System.Numerics;
using Encrypt_Decrypt;
using System.Globalization;
using Utils;

namespace Coe_Ticket
{
    public class CoeAPI
    {
        private CoeAPIDB API;
        private Decrypt ed;
        private Response response;
        private string ConnectionString;
        public CoeAPI(string ConnectionString)
        {
            this.response = new Response();
            this.ConnectionString = ConnectionString;
            API = new CoeAPIDB(ConnectionString);
            ed = new Decrypt();
        }

        private void GetAPIList(int skip, int limit)
        {
            dynamic Page = new ExpandoObject();
            Page.data = this.API.GetAPIList(skip, limit, string.Empty);
            Page.count = 10;

            SetBodyResponse("Get Deploy List", string.Empty, Page);
        }

        private void GetAPI(string id)
        {
            SetBodyResponse("Get Deploy List", string.Empty, this.API.GetAPI(id));
        }

        private void UpdateAPI(dynamic UpdatedAPI)
        {
            SetBodyResponse("Update deploy", string.Empty, this.API.UpdateAPI(UpdatedAPI));
        }

        private void CreateAPI(dynamic NewAPI)
        {
            dynamic API = new ExpandoObject();
            API._id = Convert.ToString(new BigInteger(Guid.NewGuid().ToByteArray()));

            SetBodyResponse("Get Deploy List", string.Empty, this.API.CreateDeploy(API));
        }

        private void DeleteAPI(string idAPI)
        {
            SetBodyResponse("Delete Deploy", string.Empty, this.API.DeleteDeploy(idAPI));
        }

        private void SetBodyResponse(string Message, string user, dynamic Body)
        {
            this.response.message = Message;
            this.response.user = user;
            this.response.status = 200;
            this.response.body = Body;
        }

        public Response ExecuteMethod(string Resource, dynamic Params)
        {
            try
            {
                switch (Resource.ToUpper())
                {
                    case "GETAPILIST":
                        GetAPIList(Params.skip, Params.limit);
                        break;
                    case "UPDATEAPI":
                        UpdateAPI(Params);
                        break;
                    case "CREATEAPI":
                        CreateAPI(Params);
                        break;
                    case "GETDEPLOY":
                        GetAPI(Convert.ToString(Params.id));
                        break;
                    case "DELETE":
                        DeleteAPI(Convert.ToString(Params.id));
                        break;
                    default:
                        return "Method: " + Resource.ToUpper() + ", Parameters: " + Convert.ToString((Params));
                }
            }
            catch (Exception ex)
            {
                this.response.error = ex.Message;
                this.response.status = 500;
            }
            return this.response;
        }
    }
}