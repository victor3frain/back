using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Coe_Ticket
{
    public class NotificationHub: Hub<IDeployNotification>{
        public async Task SendNotification(Notification notification){
            await Clients.All.RecieveNotification(notification);
        }
    }
}