using System.Threading.Tasks;

namespace Coe_Ticket
{
    public interface IDeployNotification
    {
        Task RecieveNotification(Notification notification);
    }
}